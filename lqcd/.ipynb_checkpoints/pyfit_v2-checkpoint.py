import basic
import numpy as np
import mpyfit as mpf


#tJ dependance functions

def Delta_tJ_8(dE,tJ):
    return np.exp(dE*(8-tJ)) + np.exp(dE*tJ)
def Delta_tJ_10(dE,tJ):
    return np.exp(dE*(10-tJ)) + np.exp(dE*tJ)
def Delta_tJ_12(dE,tJ):
    return np.exp(dE*(12-tJ)) + np.exp(dE*tJ)

#def fit_FF_8(p,x):
#    return p[0] - np.exp(2.0*p[2])*np.exp(-p[1])*Delta_tJ_8(p[2],x) - np.exp(2.0*p[2])*np.exp(-p[3])*Delta_tJ_8(p[4],x)
#def fit_FF_10(p,x):
#    return p[0] - np.exp(-p[1])*Delta_tJ_10(p[2],x) - np.exp(-p[3])*Delta_tJ_10(p[4],x)
#def fit_FF_12(p,x):
#    return p[0] - np.exp(-2.0*p[2])*np.exp(-p[1])*Delta_tJ_12(p[2],x) - np.exp(-2.0*p[2])*np.exp(-p[3])*Delta_tJ_12(p[4],x)

def fit_FF_8(p,x):
    return p[0] - p[1]*Delta_tJ_8(p[2],x) - p[3]*Delta_tJ_8(p[4],x)
def fit_FF_10(p,x):
    return p[0] - p[1]*Delta_tJ_10(p[2],x) - p[3]*Delta_tJ_10(p[4],x)
def fit_FF_12(p,x):
    return p[0] - p[1]*Delta_tJ_12(p[2],x) - p[3]*Delta_tJ_12(p[4],x)


def make_invCorr(N,Nkonf,y_J,y_avg):
    #construct correlation matrix
    yCorr = np.zeros((N,N),dtype=np.float64)
    for i1 in range(N):
        for i2 in range(N):
            for konf in range(Nkonf):
                yCorr[i1,i2] += (y_J[konf,i1] - y_avg[i1])*(y_J[konf,i2] - y_avg[i2])

    #get the diagonal values for the correlation matrix
    sigma=np.zeros(N,dtype=np.float64)
    for i in range(N):
        sigma[i] = np.sqrt(yCorr[i,i])

    #normalizing correlation matrix, correcting chi2 later
    yCorr_norm=np.zeros((N,N),dtype=np.float64)
    for i1 in range(N):
        for i2 in range(N):
            yCorr_norm[i1,i2] = yCorr[i1,i2]/(sigma[i1]*sigma[i2])

    #calculate pseudoinverse of yCorr_norm
    invCorr=np.linalg.pinv(yCorr_norm,rcond=1e-15)

    return invCorr,sigma

def join_deltaT(dt_list,dt_fit,F3,funDelta):
    #first join data in one array
    #second create an array of functions to be fitted on each data
    nn=0
    for dt in dt_list:
        ttmin = int(dt/2 - dt_fit[dt])
        ttmax = int(dt/2 + dt_fit[dt])

        tmp_tidx = [i for i in range(ttmin,ttmax+1)]
        tmp_dataF_t = np.asarray(F3[dt][ttmin:ttmax+1])
        tmp_funF_t = [funDelta[str(dt)] for i in range(ttmax+1-ttmin)]

        if nn==0:
            temp = tmp_dataF_t
            ftemp = tmp_funF_t
            itmp = tmp_tidx
        else:
            temp = np.hstack([dataF_t,tmp_dataF_t])
            ftemp = np.hstack([funF_t,tmp_funF_t])
            itmp = np.hstack([tidx,tmp_tidx])

        dataF_t=temp
        funF_t=ftemp
        tidx=itmp

        nn+=1
    return dataF_t,funF_t,itmp

def chi2_fit_FF(p,args):
    #f is a pointer to the function, which accepts the following arguments:
    #def f(x,p):
    #   return p[0]*x+p[1]*x**2

    y, flist, y_idx, inv_cov, sig_y, N = args
    i1=1
    chi2_sum=0.0
    for i1 in range(N):
        for i2 in range(N):
            chi2_sum += (flist[i1](p,y_idx[i1]) - y[i1])*inv_cov[i1,i2]*(flist[i2](p,y_idx[i2]) - y[i2])/(sig_y[i1]*sig_y[i2])

    ndf=float(N-len(p))
    return np.sqrt(chi2_sum/ndf)

def fit_FF_multi(N, Nkonf, FF_av, fun_FF, tidx, iCor, sig_FF, pinit):
    args=(FF_av, fun_FF, tidx, iCor, sig_FF, N)
    pfit, results = mpf.fit(chi2_fit_FF,pinit,args)

    chi2 = chi2_fit_FF(pinit,args)**2
    succ = results
    return chi2,pfit,succ


###################################################################################
#########################       one exponential fit       #########################
###################################################################################

def f_one_exp(x,p,toff):
    return p[0]*np.exp(-p[1]*(x+toff))

def chi2_fit_one_exp(p,args):
    #f is a pointer to the function, which accepts the following arguments:
    #def f(x,p):
    #   return p[0]*x+p[1]*x**2

    y, f, inv_cov, sig_y, N, t0 = args

    chi2_sum=0.0
    for i1 in range(N):
        for i2 in range(N):
            chi2_sum += (f(i1,p,t0) - y[i1])*inv_cov[i1,i2]*(f(i2,p,t0) - y[i2])/(sig_y[i1]*sig_y[i2])

    ndf=float(N-len(p))
    return np.sqrt(chi2_sum/ndf)

def fit_eval(N,Nkonf,yJ,y,iCor,sig,pinit,tzero):
    pp=(y,f_one_exp,iCor,sig,N,tzero)

    pfit, results = mpf.fit(chi2_fit_one_exp, pinit, pp)
    res = pfit
    chi2 = chi2_fit_one_exp(pfit,pp)**2
    succ = results['status'][0]
    return chi2,res,results
