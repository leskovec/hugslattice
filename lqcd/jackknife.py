'''
  This python module contains the jackknife routines
'''

import numpy as np
from collections import defaultdict
def rec_dd():
  return defaultdict(rec_dd)

class form_factor:
  def __init__(self,pi,pf,Qc):
    self.pi = np.asarray(pi)
    self.pf = np.asarray(pf)
    self.Qc = np.asarray(Qc)
    self.ff = np.zeros((4),dtype='complex128')
    self.Q2 = 0.0

  def assign_ff(self,ff,idx):
    self.ff[idx] = ff

  def assign_Q2(self,Q2):
    self.Q2 = Q2

def jack_cv_ff(conf_list,x):
    Nkonf=len(conf_list)
    i0=conf_list[0]
    cv=form_factor(x[i0].pi,x[i0].pf,x[i0].Qc)
    tmpf=np.zeros(4,dtype='complex128')
    for idx in range(4):
      tmpQ2=0.
      for konf in conf_list:
        tmpQ2 += x[konf].Q2
        tmpf[idx] += x[konf].ff[idx]
      cv.ff[idx]=tmpf[idx]/Nkonf
      cv.Q2=tmpQ2/Nkonf
    return cv
  
def jack_resample_ff(conf_list,x):
    xJ=rec_dd()
    Nkonf=len(conf_list)
    i0=conf_list[0]
    for ex_konf in conf_list:
      xtt=form_factor(x[i0].pi,x[i0].pf,x[i0].Qc)
      tmpf=np.zeros(4,dtype='complex128')
      for idx in range(4):
        for konf in conf_list:
          if konf!=ex_konf:
            tmpf[idx] += x[konf].ff[idx]
        xtt.ff[idx] = tmpf[idx]/(Nkonf-1)
      tmpQ2=0.
      for konf in conf_list:
        tmpQ2+=x[konf].Q2
      xtt.Q2=tmpQ2/(Nkonf-1)
      xJ[ex_konf]=xtt
    return xJ

def jack_stats_ff(conf_list,x):
    Nkonf=len(conf_list)
    x_avg=jack_cv_ff(conf_list,x) 
    i0=conf_list[0]
    sig_x=form_factor(x[i0].pi,x[i0].pf,x[i0].Qc)
    
    tmpfR=np.zeros(4,dtype='double')
    tmpfI=np.zeros(4,dtype='double')
    for idx in range(4):
      for konf in conf_list:
        tmpfR[idx] += (x[konf].ff[idx].real - x_avg.ff[idx].real)**2
        tmpfI[idx] += (x[konf].ff[idx].imag - x_avg.ff[idx].imag)**2
      sig_x.assign_ff((np.sqrt(tmpfR[idx])+1j*np.sqrt(tmpfI[idx]))*(Nkonf-1)/(Nkonf),idx)
    
    tmpQ2=0.
    for konf in conf_list:
      tmpQ2 += (x[konf].Q2 - x_avg.Q2)**2
    sig_x.assign_Q2(np.sqrt(tmpQ2)*(Nkonf-1)/(Nkonf))
    return x_avg,sig_x

def jack_average_scalar(x):
  avg = 0.
  avg = np.sum(x)/float(x.shape[0])
  return avg

def jack_sigma_scalar(x):
  n = x.shape[0]
  x_avg = np.sum(x)/float(n)
  simga_x2 = 0.
  sigma_x2 = np.sum(np.power(np.add(x, - x_avg),2)) 
  sigma = np.sqrt(sigma_x2)*np.sqrt((float(n)-1.)/float(n))
  return x_avg,sigma

def jack_resample_scalar(x):
  n = x.shape[0]
  idx = np.arange(n) 
  x_J = np.zeros((n),dtype='float64')
  for iextr in range(n):
    x_J[iextr] = np.sum(x[idx!=iextr])
  x_J=x_J/float(n-1.)
  return x_J
